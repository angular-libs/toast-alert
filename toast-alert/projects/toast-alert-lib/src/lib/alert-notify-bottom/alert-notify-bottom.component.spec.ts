import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertNotifyBottomComponent } from './alert-notify-bottom.component';

describe('AlertNotifyBottomComponent', () => {
  let component: AlertNotifyBottomComponent;
  let fixture: ComponentFixture<AlertNotifyBottomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlertNotifyBottomComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertNotifyBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
