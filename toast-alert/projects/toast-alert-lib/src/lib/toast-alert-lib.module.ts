import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ToastNotifyService } from './toast-notify/toast-notify.service';
import { ToastNotifyComponent } from './toast-notify/toast-notify.component';
import { AlertNotifyBottomComponent } from './alert-notify-bottom/alert-notify-bottom.component';
import { AlertNotifyService } from './alert-notify-bottom/alert-notify.service';



@NgModule({
  declarations: [
    ToastNotifyComponent,
    AlertNotifyBottomComponent
  ],
  imports: [
    BrowserModule
  ],
  exports: [
    ToastNotifyComponent,
    AlertNotifyBottomComponent
  ],
  providers: [
    ToastNotifyService,
    AlertNotifyService
  ]
})
export class ToastAlertLibModule { }
