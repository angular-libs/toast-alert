import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { IAlert, IButton } from "./alert-notify-button.model";

@Injectable()
export class AlertNotifyService{

    $alertNotificationSend= new Subject<IAlert>();
    $alertNotificationReceive=new Subject<IButton>();
    constructor(){}

    sendAlert(header: string, message: string, buttons: IButton[]): void{
        this.$alertNotificationSend.next({
            button: buttons,
            header: header,
            message: message
        });
    }
} 