import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { ToastNotifyIconEnum } from "./toast-notify-icon.enum";
import { ToastNotifyModel } from "./toast-notify.model";


@Injectable()
export class ToastNotifyService {

    $notification: Subject<ToastNotifyModel>;

    constructor() {
        this.$notification = new Subject<ToastNotifyModel>();
    }
    /**
     * Show the success message
     * @param header Header for the Toast
     * @param message Message in the toast
     * @param id Remove loader then pass the loader id to show the success
     * @param imgPath if you want to add image on top of toast
     */
    success(header: string, message: string, id?: string, imgPath?: string) {
        this.$notification.next(new ToastNotifyModel(id, header, message, ToastNotifyIconEnum.success, null, imgPath));
    }

    /**
     * Show the warning message
     * @param header Header for the Toast
     * @param message Message in the toast
     * @param id Remove loader then pass the loader id to show the success
     * @param imgPath if you want to add image on top of toast
     */
    warning(header: string, message: string, id?: string, imgPath?: string) {
        this.$notification.next(new ToastNotifyModel(id, header, message, ToastNotifyIconEnum.warning, null, imgPath));
    }

    /**
     * Show the information message
     * @param header Header for the Toast
     * @param message Message in the toast
     * @param id Remove loader then pass the loader id to show the success
     * @param imgPath if you want to add image on top of toast
     */
    information(header: string, message: string, id?: string, imgPath?: string) {
        this.$notification.next(new ToastNotifyModel(id, header, message, ToastNotifyIconEnum.info, null, imgPath));
    }

    /**
     * Show the error message
     * @param header Header for the Toast
     * @param message Message in the toast
     * @param id Remove loader then pass the loader id to show the success
     * @param imgPath if you want to add image on top of toast
     */
    error(header: string, message: string, id?: string, imgPath?: string) {
        this.$notification.next(new ToastNotifyModel(id, header, message, ToastNotifyIconEnum.error, null, imgPath));
    }
    /**
   * Show the error message
   * @param header Header for the Toast
   * @param message Message in the toast
   * @param id Remove loader then pass the loader id to show the success
   * @param imgPath if you want to add image on top of toast
   */
    question(header: string, message: string, id?: string, imgPath?: string) {
        this.$notification.next(new ToastNotifyModel(id, header, message, ToastNotifyIconEnum.question, null, imgPath));
    }
    /**
     * Show loader in the toast
     * @param id Id to revove loader for the future by passing id in the success, error, warning or removeLoading method
     * @param header Header for the Toast
     * @param message Message in the toast
     * @param imgPath if you want to add image on top of toast
     */
    loading(id: string, header?: string, message?: string, imgPath?: string) {
        this.$notification.next(new ToastNotifyModel(id, (!header ? 'Loading...' : header), (!message ? 'Loading the record' : message), ToastNotifyIconEnum.loading, null, imgPath));
    }
    /**
     * Stop or remove loader
     * @param id Pass Loader id to stop the loading automatically.
     */
    stopLoading(id: string) {
        this.$notification.next(new ToastNotifyModel(id, null, null, null, true));
    }
}