import { ButtonColorEnum } from "./alert-notity-button-color.enum";

export interface IAlert{
    header: string;
    message: string;
    button: IButton[];
}


export interface IButton{
    name: string;
    type: ButtonColorEnum
}