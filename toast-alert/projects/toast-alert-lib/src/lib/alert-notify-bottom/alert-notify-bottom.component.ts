import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import { take } from 'rxjs/operators';
import { IAlert, IButton } from './alert-notify-button.model';
import { AlertNotifyService } from './alert-notify.service';

@Component({
  selector: 'lib-alert-notify-bottom',
  templateUrl: './alert-notify-bottom.component.html',
  styleUrls: ['./alert-notify-bottom.component.css']
})
export class AlertNotifyBottomComponent implements OnInit {

  showBox=false;
  get showAlert(): boolean{
return !!this.alert; 
  }
  alert: IAlert;
  constructor(private alertService: AlertNotifyService) { }

  ngOnInit(): void {
this.alertService.$alertNotificationSend.subscribe(res=>{
  if(res && res.button?.length>0){
    
    this.alert=res; 
    this.showBox=!!this.alert;
  }
});
  }


  closeWindow(){
    this.clearMessage(this.alert.button[this.alert.button.length-1]);
  }

  clearMessage(button: IButton){
    this.alert=null;
  interval(400).pipe(take(1)).subscribe(()=>{
    this.showBox=false;
  });
    
    this.alertService.$alertNotificationReceive.next(button);
  }

  selectedOption(button: IButton){
  this.clearMessage(button);
  }

}
