// import { stringify } from '@angular/compiler/src/util';
import { Component } from '@angular/core';
import { AlertNotifyService, ToastNotifyService, ButtonColorEnum } from 'toast-alert-lib';
import { take } from 'rxjs/operators';

@Component({ 
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  loadingVal = "1";
  constructor(private notify: ToastNotifyService, private alertService: AlertNotifyService) {

  }


  addValue() {

    this.notify.success('Success', 'test message');
  }
  Loading() {
    this.notify.loading(this.loadingVal, null, 'Saving your records');
  }
  Success() {

    this.notify.success('Success', 'Record saved successfully', this.loadingVal, 'https://www.voicesofyouth.org/sites/voy/files/images/2020-07/success.jpg');
  }
  removeLoadin() {

    this.notify.stopLoading(this.loadingVal);
  }

  Warning() {

    this.notify.warning('Warning', 'Valid information not passed');
  }
  Error() {

    this.notify.error('Error', 'Record not saved');
  }
  Information() {

    this.notify.information('Information', 'You are a good person');
  } 
  Question() {

    this.notify.question('Question', 'are you fine?');
  }

  alertMess(){
     this.alertService.sendAlert('Danger Message', 'This is a danger message', [{name:'Close', type: ButtonColorEnum.Other}]);
    this.alertService.$alertNotificationReceive.pipe(take(1)).subscribe(res=>{
      alert(res.name);
    })
  }

}
