# ToastAlertLib

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.9.


## BitBucket Repo

This library sample is available in [Bitbucket](https://babitaraheja@bitbucket.org/angular-libs/toast-alert.git).


## How to use:

Install: npm i toast-alert-lib

    Go to app.component.html
            For toast notification
        <lib-toast-notify></lib-toast-notify>
            For alert messages add this message
        <lib-alert-notify-bottom></lib-alert-notify-bottom>

    In your code:
                        -- for toast service                            --for alert notification
          constructor(private notify: ToastNotifyService, private alertService: AlertNotifyService) {}

        
        

#Toast Service Call

loadingVal = "1";

  Loading() {
    this.notify.loading(this.loadingVal, null, 'Saving your records');
  }
  Success() {

    this.notify.success('Success', 'Record saved successfully', this.loadingVal, 'https://www.voicesofyouth.org/sites/voy/files/images/2020-07/success.jpg');
  }
  removeLoading() {

    this.notify.stopLoading(this.loadingVal);
  }

  Warning() {

    this.notify.warning('Warning', 'Valid information not passed');
  }
  Error() {

    this.notify.error('Error', 'Record not saved');
  }
  Information() {

    this.notify.information('Information', 'You are a good person');
  } 
  Question() {

    this.notify.question('Question', 'are you fine?');
  }




#Alert Service

  alertMess(){
     this.alertService.sendAlert('Danger Message', 'This is a danger message', [{name:'Close', type: ButtonColorEnum.Other}]);
    this.alertService.$alertNotificationReceive.pipe(take(1)).subscribe(res=>{
      alert(res.name);
    })
  }
