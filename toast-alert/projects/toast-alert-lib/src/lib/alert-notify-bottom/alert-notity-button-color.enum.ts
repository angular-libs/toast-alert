export enum ButtonColorEnum{
    Success = "is-success",
    Error = "is-danger",
    Warning = "is-warning",
    Information = "is-info",
    Link = "is-link",
    Success_Light = "is-primary",
    Other = ""
}