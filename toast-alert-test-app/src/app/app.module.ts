import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ToastAlertLibModule } from 'toast-alert-lib';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ToastAlertLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
